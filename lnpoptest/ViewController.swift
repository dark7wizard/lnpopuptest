//
//  ViewController.swift
//  lnpoptest
//
//  Created by Mohammed Ashour on 10/2/20.
//

import UIKit
import LNPopupController

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func showpop(_ sender: Any) {
        let mPlayer = storyboard?.instantiateViewController(withIdentifier: "vc") as! UIViewController
        navigationController?.popupBar.marqueeScrollEnabled = true
        navigationController?.popupBar.tintColor = .black
        navigationController?.popupBar.progressViewStyle = .bottom
        navigationController?.presentPopupBar(withContentViewController: mPlayer, openPopup: true, animated: true, completion: nil)
    }
    
}

